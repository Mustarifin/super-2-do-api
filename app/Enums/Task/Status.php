<?php

namespace App\Enums\Task;

use BenSampo\Enum\Enum;

/**
 * @method static static Active()
 * @method static static Completed()
 */
final class Status extends Enum
{
    const Active    = 1;
    const Completed = 2;
}
