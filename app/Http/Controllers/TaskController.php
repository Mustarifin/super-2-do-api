<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Request;
use App\Http\Resources\TaskResponse;
use App\Models\Filter\TaskFilter;
use App\Http\Requests\Task\IndexRequest;
use App\Http\Requests\Task\UpdateRequest;
use App\Http\Requests\Task\DeleteRequest;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(IndexRequest $request)
    {
        $tasks = Task::filter(new TaskFilter($request))->get();

        return TaskResponse::collection($tasks);
    }

    public function count()
    {
        $tasks = new Task();

        return response()->json([
            'status' => 'Ok',
            'active'  => $tasks->where('status', 1)->count(),
            'completed' => $tasks->where('status', 2)->count()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $task = Task::create($request->all());

        return new TaskResponse($task);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function updateBulk(UpdateRequest $request)
    {
        $tasks = Task::where('status', $request->status)->update(['status' => $request->status == 1 ? 2 : 1]);

        return response()->json([
            'status' => 'Ok',
            'message' => 'Success Update Data'
        ]);
    }

    public function update(UpdateRequest $request)
    {
        $tasks = Task::where('id', $request->id)->update(['status' => $request->status]);

        return response()->json([
            'status' => 'Ok',
            'message' => 'Success Update Data'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroyBulk(Request $request)
    {
        $tasks = Task::whereIn('id', $request->ids)->delete();

        return response()->json([
            'status' => 'Ok',
            'message' => 'Success Update Data'
        ]);
    }

    public function destroy(Request $request)
    {
        $tasks = Task::where('id', $request->id)->delete();

        return response()->json([
            'status' => 'Ok',
            'message' => 'Success Update Data'
        ]);
    }
}
