<?php

namespace App\Models\Filter;

use App\Traits\Filter\Filter;
use Illuminate\Database\Eloquent\Builder;

class TaskFilter extends Filter
{
    public function status($status): Builder
    {
        return $this->builder->where('status', $status);
    }
}