### Installing

A step by step series of examples that tell you how to get a development env running

1.  `$ git clone <your repo>`
2.  `$ composer install`
3.  Create **.env** file as per **.env.example**. #REQUIRED line must be change
4.  `$ php artisan key:generate`/ storage to storage / app / public)
5.  `$ php artisan migrate`
7.  Set DocumentRoot to {PROJECT_HOME} / public OR run `$ php artisan serve`
6.  Access from Browser / default `localhost:8000`